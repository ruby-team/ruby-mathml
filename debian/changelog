ruby-mathml (1.0.0-2) unstable; urgency=medium

  * Use github as upstream
  * Import spec files (Temporary, drop next version bump)
  * Remove X?-Ruby-Versions fields from d/control

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 04 Feb 2025 18:50:12 +0900

ruby-mathml (1.0.0-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Add .gitattributes to keep unwanted files out of the source package

  [ Lucas Kanashiro ]
  * New upstream version 1.0.0
  * Bump debhelper compat level to 13
  * Declare compliance with Debian Policy 4.7.0
  * Runtime depends on  instead of ruby interpreter
  * Remove all patches, not needed anymore

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 16 Dec 2024 21:57:40 -0300

ruby-mathml (0.14-5) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Cédric Boutillier ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use https:// in Vcs-* fields
  * Move debian/watch to gemwatch.debian.net
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields
  * Add patches to fix tests with ruby2.7 (Closes: #952029)
    + concatenation of string litterals now frozen
    + $SAFE/taint mechanism removed
  * Drop compat file, rely on debhelper-compat and bump compat level to 12
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.

 -- Cédric Boutillier <boutil@debian.org>  Tue, 25 Feb 2020 22:48:40 +0100

ruby-mathml (0.14-4) unstable; urgency=medium

  * Team upload.
  * Fix tests for rspec version 3.3.0.
  * Fix custom matcher definition for rspec version 3.3.0.
  * Fix test require for autopkgtest.
  * debian/compat: Bump to bersion 9, silence lintian warning.
  * debian/control: Update vcs-git link to https, silence lintian warning.
  * debian/control: Update vcs-browser link to https, silence lintian warning.
  * debian/control: Update standards-version to 3.9.7, silence lintian
                    warning.
  * debian/control: Update deb-helper to require version >= 9.

 -- Lucas Albuquerque Medeiros de Moura <lucas.moura128@gmail.com>  Wed, 02 Mar 2016 09:10:20 -0300

ruby-mathml (0.14-3) unstable; urgency=medium

  * Bump standard version: 3.9.6 (not changed)
  * Rebuild against newer ruby for update rubygems-integration information

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 28 Oct 2014 19:14:56 +0900

ruby-mathml (0.14-2) unstable; urgency=medium

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag
  * use canonical URI in Vcs-* fields
  * debian/copyright: use DEP5 copyright-format/1.0
    official URL for Format field

  [ Youhei SASAKI ]
  * Bump Standard Version: 3.9.5
  * Drop Transitional Packages (Closes: #735720)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 19 Jan 2014 23:49:18 +0900

ruby-mathml (0.14-1) unstable; urgency=low

  * New upstream release: 0.14
  * Fix rdoc options

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 07 Dec 2012 22:58:27 +0900

ruby-mathml (0.12.2-2) unstable; urgency=low

  * Team upload.
  * Bump build dependency on gem2deb to >= 0.3.0~.
  * Override lintian warning about duplicate description of transitional
    packages
  * Use Breaks instead of Conflicts for dependency relation on old packages
  * use rdoc instead of rdoc1.8 to build doc
  * Depend on libjs-jquery and symlink jquery.js for the docs
  * Register rdoc documentation with doc-base

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Sat, 30 Jun 2012 11:59:50 +0200

ruby-mathml (0.12.2-1) unstable; urgency=low

  * Imported Upstream version 0.12.2
  * Update watch: use gemwatch redirector
  * Drop patches: merged upstream

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sun, 25 Mar 2012 14:18:52 +0900

ruby-mathml (0.11-3) unstable; urgency=low

  * Unapply patches after build
  * Bump Standard Version: 3.9.3
  * Update Patches: 0001-Fix-Rake-Backword-Compatibility.patch
  * New patch 0002-Fix-RSpec-Matcher-Parse-Error.patch
    - Fixup Matcher for FTBFS. (Closes: #665257)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 23 Mar 2012 13:13:31 +0900

ruby-mathml (0.11-2) unstable; urgency=low

  * Add Vcs-Git: move pkg-ruby-extras Git repo.
  * Add ruby-tests.rake

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 24 Jan 2012 01:52:50 +0900

ruby-mathml (0.11-1) unstable; urgency=low

  * New upstream version: 0.11
  * Bump Standard version: 3.9.2
  * Switch to gem2deb based packageing
    - Source and binary packages are renamed to ruby-mathml
    - Crate transitional packages
  * Add ruby-eim-xml to Build-Depends for running "rake spec"
  * Change copyright format: Dep5

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Mon, 25 Jul 2011 19:52:42 +0900

libmathml-ruby (0.10-1) unstable; urgency=low

  * New Upstream version 0.10
  * add watch file

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Sat, 26 Feb 2011 00:29:21 +0900

libmathml-ruby (0.9-1) unstable; urgency=low

  * Initial release (Closes: #602206)

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 02 Nov 2010 23:27:19 +0900
